app.controller('PostsController', function ($scope, $rootScope, Post, $ionicLoading, $stateParams, Category) {
  $scope.showPosts = true;
  $scope.showSeach = false;

  $scope.infiniteScroll = false
  $scope.page = 1
  $scope.category = $stateParams.category;
  $scope.posts = []

  setTitle()

  console.log("PostsController : " + $scope.page + " : " + $stateParams.category)

  Post.posts($scope.page, $scope.category).then(function (data) {
    if ($scope.page == 1) {
      $scope.posts = []
    }

    console.log(data)

    $scope.posts = $scope.posts.concat(data);

    $scope.hasData = true;
    $scope.$broadcast('scroll.infiniteScrollComplete')
    $scope.page++
    $scope.infiniteScroll = true
  }, function (reason) {
    console.log("POST PAGE : 1 - CATEGORY " + $stateParams.category + " : " + reason)
  })

  $scope.loadMore = function () {
    if ($scope.infiniteScroll) {
      console.log("infiniteScroll " + "POST PAGE : " + $scope.page + " - CATEGORY " + $scope.category)
      Post.posts($scope.page, $scope.category).then(function (data) {
        $scope.posts = $scope.posts.concat(data);

        $scope.$broadcast('scroll.infiniteScrollComplete')
        $scope.page++
      }, function (reason) {
        console.log("ERROR infiniteScroll POST PAGE : " + $scope.page + " - CATEGORY " + $stateParams.category + " : " + reason)
      })
    }

  }

  $scope.$on("$ionicView.beforeLeave", function () {
    $scope.infiniteScroll = false;
  })

  $scope.$on("$ionicView.enter", function () {
    setTitle()
    $scope.infiniteScroll = true;
  })

  $scope.$on('$stateChangeSuccess', function () {
    $scope.loadMore();
  })

  function setTitle(){
    if ($scope.category == "all") {
      $rootScope.$broadcast('header:title', "PAS")
    } else {
      $rootScope.$broadcast('header:title', "PAS - " + Category.bySlug($scope.category))
    }
  }

  $scope.openLink = function (url) {
    openLink(url)
  }

})

app.controller('PostController', function ($scope, Post, $cordovaSocialSharing, $stateParams) {

  $scope.$on("$ionicView.afterEnter", function () {
    $scope.post = Post.get($stateParams.id)
  })

  $scope.openLink = function () {
    window.open($scope.post.url, '_system')
  }

  $scope.share = function () {
    $cordovaSocialSharing
      .share(null, null, null, $scope.post.url)
      .then(function (result) {
        // Success!
      })
  }
})

function openLink(url) {
  window.open(url, '_system')
};

