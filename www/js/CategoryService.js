app.factory('Category', function ($http, $rootScope, $q) {

  var CATEGORY_URL = "http://www.partiantisioniste.com/wp-json/wp/v2/categories";
  var categories = {};

  function addCategories(categoryList){
    categoryList.forEach(function(category){
      categories[category.slug] = category.name
    })
  }

  return {

    getAll: function () {
      console.log("Get categories")

      var deferred = $q.defer()

      $http({
        method: 'GET',
        url: CATEGORY_URL
      }).success(function (data) {
        addCategories(data)
        deferred.resolve(data)
      }).error(function (response, status, headers, config) {
        deferred.resolve(posts)
      })

      return deferred.promise
    },

    bySlug : function(slug){
      return categories[slug]
    }
  }
})

