// Ionic Starter App

var app = angular.module('starter', ['ionic', 'ngResource', 'ngCordova'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.backButton.text("");
    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    $ionicConfigProvider.views.transition('none');
    //$ionicConfigProvider.views.maxCache(0);

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl',

        resolve: {
          categories: function (Category) {
            return Category.getAll();
          }
        }
      })

      .state('app.posts', {
        url: "/posts/:category",
        views: {
          'menuContent': {
            templateUrl: "templates/posts.html",
            controller: 'PostsController'
          }
        }
      })

      .state('app.search', {
        url: "/search/:query",
        views: {
          'menuContent': {
            templateUrl: "templates/search.html",
            controller: 'SearchController'
          }
        }
      })

      .state('app.post', {
        cache: false,
        url: "/post/:id",
        views: {
          'menuContent': {
            templateUrl: "templates/post.html",
            controller: 'PostController'
          }
        }
      })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/posts/all');

  });

app.filter('startFrom', function () {
  return function (input, start) {
    if (input) {
      start = +start; //parse to int
      return input.slice(start);
    }
    return [];
  }
})

app.directive('ngEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if(event.which === 13) {
        scope.$apply(function(){
          scope.$eval(attrs.ngEnter);
        });

        event.preventDefault();
      }
    });
  };
})

app.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      $timeout(function() {
        element[0].focus();
      }, 150);
    }
  };
});
