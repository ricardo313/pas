app.controller('SearchController', function ($scope, Post, $ionicLoading, $stateParams, $rootScope) {
  $scope.posts = []
  $scope.page = 1
  $scope.infiniteScroll = false

  $scope.$on("$ionicView.enter", function () {
    console.log("enter search")
    $rootScope.$broadcast('header:title', "PAS - " + $stateParams.query)
  })


  Post.search($scope.page, $stateParams.query).then(function (data) {
    $scope.posts = $scope.posts.concat(data);

    $scope.$broadcast('scroll.infiniteScrollComplete');
    $scope.page++;
    $scope.infiniteScroll = true;
  });

  $scope.loadMore = function () {
    if ($scope.infiniteScroll) {
      Post.search($scope.page, $stateParams.query).then(function (data) {
        $scope.posts = $scope.posts.concat(data);

        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.page++;
      });
    }

  };

});


