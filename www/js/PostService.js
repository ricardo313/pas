app.factory('Post', function ($http, $rootScope, $q) {

  var POST_URL = "http://www.partiantisioniste.com/wp-json/wp/v2/posts";
  var posts = {};

  function addPosts(data) {
    var postList = [];
    data.forEach(function (post) {
      if (posts[post.id] == undefined) {
        posts[post.id] = createPost(post);
        postList.push(posts[post.id])
      }
    })

    return postList;
  }

  function createPost(post) {
    var image, thumbnail;
    if (post._embedded['wp:featuredmedia'] == undefined) {
      image = "img/default.gif"
      thumbnail = "img/default.gif"
    } else {
      image = post._embedded['wp:featuredmedia'][0].source_url;
      if (post._embedded['wp:featuredmedia'][0].media_details.sizes.medium != undefined) {
        image = post._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url
      }

      thumbnail = post._embedded['wp:featuredmedia'][0].source_url;
      if (post._embedded['wp:featuredmedia'][0].media_details.sizes.thumbnail != undefined) {
        thumbnail = post._embedded['wp:featuredmedia'][0].media_details.sizes.thumbnail.source_url
      }
    }

    return {
      id: post.id,
      title: post.title.rendered,
      description: post.excerpt.rendered,
      date: post.date,
      image: image,
      thumbnail: thumbnail,
      url: post.link
    }
  }

  return {

    posts: function (page, category) {
      console.log("POST PAGE : " + page + " - CATEGORY " + category);

      var deferred = $q.defer();

      if (category == "all") {
        $http({
          method: 'GET',
          url: POST_URL,
          params: {
            page: page,
            _embed: ""
          }
        }).success(function (data) {
          var newPosts = addPosts(data);
          deferred.resolve(newPosts);
        }).error(function (response, status, headers, config) {
          deferred.resolve(posts)
        });
      } else {
        $http({
          method: 'GET',
          url: POST_URL,
          params: {
            page: page,
            "filter[category_name]": category,
            _embed: ""
          }
        }).success(function (data) {
          var newPosts = addPosts(data);
          deferred.resolve(newPosts);
        }).error(function (response, status, headers, config) {
          deferred.resolve(posts)
        });
      }

      return deferred.promise;
    },

    search: function (page, query) {
      console.log("post search : " + page + " : " + query)

      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: POST_URL,
        params: {
          page: page,
          "filter[s]": query,
          _embed: ""
        }
      }).success(function (data) {
        var newPosts = addPosts(data);
        deferred.resolve(newPosts);
      }).error(function (response, status, headers, config) {
        deferred.resolve(posts)
      });

      return deferred.promise;
    },

    get: function (id) {
      return posts[id]
    }
  }
})
