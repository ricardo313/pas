app.controller('AppCtrl', function ($scope, $state, categories,$timeout) {
  $scope.search = {
    query : ""
  }

  $scope.$on('header:title', function (event, data) {
    $scope.title = data
  })

  $scope.showSearch = function () {
    $scope.search.query = "";
    $scope.hasSearch = true;

    var oInput = document.getElementById("search");
    oInput.focus();
  }

  $scope.search = function (query) {
    $scope.hasSearch = false;
    $state.go("app.search", { query: query});
  }

  $scope.hasSearch = false;
  $scope.categories = categories;

  $scope.closeSearch = function () {
    $scope.hasSearch = false;
  }

});

